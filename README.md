# Fully Typed Function Composition Method

A simple, zero-dependency, type-safed function composition method without function length limitation!

## Usage

```ts
import { compose } from "typed-compose";

const numberToString = (val: number): string => val.toString();
const stringToBoolean = (val: string): boolean => !!val;

const numberToBoolean = compose(numberToString, stringToBoolean);
```

```ts
function hexToRgb(hex: `#${string}`): `rgb(${number}, ${number}, ${number})` { ... }
function rgbToHsl(rgb: `rgb(${number}, ${number}, ${number})`): `hsl(${number}, ${number}%, ${number}%)` { ... }
function hslToLab(hsl: `hsl(${number}, ${number}%, ${number}%)`): `lab(${number}% ${number} ${number})`  { ... }

const hexToLab = compose(hexToRgb, rgbToHsl, hslToLab)
```

## Type-Safed

```ts
compose(
  (val: number) => val.toSring(),
  (val: number) => !!val // TS Validation Fail
);

compose(
  (val: number) => val.toSring(),
  (val: string) => !!val // TS Validation Pass
);
```
