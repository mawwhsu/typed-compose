type ComposeFns<V extends readonly ((...args: any[]) => any)[]> = V extends [
  infer F,
  infer S,
  ...infer R
]
  ? F extends (...args: any[]) => any
    ? S extends (arg: ReturnType<F>) => any
      ? R extends Array<(arg: any) => any>
        ? [F, ...ComposeFns<[S, ...R]>]
        : [F, S]
      : [F, (arg: ReturnType<F>) => any, ...any[]]
    : []
  : V extends []
  ? [(...args: any[]) => any]
  : V;

type ComposeArgs<V extends readonly ((...args: any[]) => any)[]> = V extends [
  infer F,
  ...infer _
]
  ? F extends (...args: any[]) => any
    ? Parameters<F>
    : []
  : [];

type ComposeReturns<V extends readonly ((...args: any[]) => any)[]> =
  V extends [...infer _, infer L]
    ? L extends (...args: any[]) => any
      ? ReturnType<L>
      : never
    : never;

export function compose<F extends readonly ((...arg: any[]) => any)[]>(
  ...fns: ComposeFns<F>
): (...args: ComposeArgs<F>) => ComposeReturns<F> {
  if (!fns.length) return (...args: any[]): any => {};

  return (...args: any[]): any => {
    let i = 0;
    let result: any = fns[i++](...args);
    while (i < fns.length) {
      result = fns[i++](result);
    }
    return result;
  };
}

export default compose;
